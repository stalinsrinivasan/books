#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/ioctl.h>
#include<fcntl.h>
#include<unistd.h>
#include<string.h>

#define BUF_SIZE 36

#define DEVICE_NAME "/dev/rdwr"

int main(int argc, char *argv[])
{
	int fd;
	char readbuf[BUF_SIZE];
	char writebuf[BUF_SIZE];
	int retval = 0;

	fd = open(DEVICE_NAME, O_RDWR); /* opening the device */
	if(fd < 0) {
		printf("status[%s]: \nError in device file opening\n", __func__);

		return -1;
	}

	printf("\nEnter the string to be transferred between kernel and user spaces: ");
	scanf("%s", writebuf);

	retval = write(fd, writebuf, BUF_SIZE);  /* writing date into kernel space */
	if(retval != 0) {
		printf("status[%s]: \nError in device file writing\n", __func__);

		return -1;
	}

	memset(readbuf, 0, BUF_SIZE);
	retval = read(fd, readbuf, BUF_SIZE); /* Reading back from kernel space */
	if(retval != 0) {
		printf("status[%s]: \nError in device file reading - retval: %d\n", __func__, retval);

		return -1;
	} else {
		printf("\nRead Buf: %s\n", readbuf); /* verifying the read data with written data confirms the transfer */
	}

	close(fd); /* closing the device */

	return 0;
}
