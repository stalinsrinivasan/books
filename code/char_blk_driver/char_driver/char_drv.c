#include<linux/init.h>
#include<linux/kernel.h>
#include<linux/module.h>
#include<linux/version.h>
#include<linux/device.h>
#include<linux/cdev.h>
#include<linux/fs.h>
#include<linux/slab.h>
#include<linux/errno.h>
#include<asm/uaccess.h>
#include<asm/system.h>

#define MINOR_NUM 0
#define MINOR_CNT 1
#define KBUF_SIZE 36

#define BLK_SIZE 4096

#define MY_DEVICE "chardev"

static dev_t dev;
int major_no = 0;
char *kbuffer;

static int chardev_open(struct inode *i, struct file *fp);
static int chardev_close(struct inode *i, struct file *fp);
ssize_t chardev_read(struct file *fp, char *buffer, size_t count, loff_t *fpos);
ssize_t chardev_write(struct file *fp, char *buffer, size_t count, loff_t *fpos);

static int chardev_open(struct inode *i, struct file *fp) {
	printk(KERN_INFO "\nstatus[%s]: File Opened\n", __func__);

	return 0;
}

static int chardev_close(struct inode *i, struct file *fp) {
	printk(KERN_INFO "\nstatus[%s]: File Closed\n", __func__);

	return 0;
}

ssize_t chardev_read(struct file *fp, char *buffer, size_t count, loff_t *fpos) {
	int bytes_read = 0;

	bytes_read = copy_to_user(buffer, kbuffer, count);
	if(bytes_read != 0) {
		printk(KERN_INFO "\nstatus[%s]: Error in reading\n", __func__);

		return -ENOMEM;
	}

	return bytes_read;
}

ssize_t chardev_write(struct file *fp, char *buffer, size_t count, loff_t *fpos) {
	int bytes_write = 0;

	bytes_write = copy_from_user(kbuffer, buffer, count);
	if(bytes_write != 0) {
		printk(KERN_INFO "\nstatus[%s]: Error in writing\n", __func__);

		return -ENOMEM;
	}

	return bytes_write;
}

static struct file_operations chardev_fops = {
	.owner = THIS_MODULE,
	.open = chardev_open,
	.release = chardev_close,
	.read = chardev_read,
	.write = chardev_write,
};

static int __init chardev_init(void) {
	int retval;

	/* requesting the device number */
	retval = register_chrdev(0, MY_DEVICE, &chardev_fops);
	if(retval < 0) {
		printk(KERN_INFO "\nstatus[%s]: Error in allocating char device\n", __func__);

		return retval;
	}
	printk(KERN_INFO "\nstatus[%s]: Major no: %d\n", __func__, retval);

	/* allocating kernel memory */
	kbuffer = kmalloc(KBUF_SIZE, GFP_KERNEL);
	memset(kbuffer, 0, KBUF_SIZE);

	return 0;
}

static void __exit chardev_exit(void) {

	/* releasing kernel memory */
	if(kbuffer) {
		kfree(kbuffer);
	}

	/* unregistering the device number */
	unregister_chrdev_region(dev, MINOR_CNT);
}

module_init(chardev_init);
module_exit(chardev_exit);

MODULE_AUTHOR("Stalin Srinivasan S <stalinsrinivasan.subramanian@in.bosch.com>");
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Memory Read/Write Driver");
