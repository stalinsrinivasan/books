#include<linux/init.h>
#include<linux/kernel.h>
#include<linux/module.h>
#include<linux/moduleparam.h>
#include<linux/version.h>
#include<linux/device.h>
#include<linux/fs.h>
#include<linux/fcntl.h>
#include<linux/slab.h>
#include<linux/vmalloc.h>
#include<linux/types.h>
#include<linux/errno.h>
#include<asm/uaccess.h>
#include<asm/system.h>

#include<linux/blkdev.h>
#include<linux/genhd.h>
#include<linux/hdreg.h>

#define KERNEL_SECTOR_SIZE	512

/* global variable declarations */
static unsigned int Majorno = 0;
static unsigned int Minorno = 0;
static unsigned int Logical_blk_size = 512; /* sector size */
module_param(Logical_blk_size, int, 0);
static unsigned int Nsectors = 1024; /* device total size */
module_param(Nsectors, int, 0);

/* structure definitions */
struct blkdev {
	int size;
	u8 *data;
	spinlock_t lock;
	struct gendisk *gd;
};

/* struct member declarations */
struct blkdev Dev;
static struct request_queue *Que;

/* function declarations */
static void blkdev_transfer(struct blkdev *dev, sector_t sector, unsigned long nsect, char *buffer, int write);
static void blkdev_request(struct request_queue *que);
int blkdev_getgeo(struct block_device *block_device, struct hd_geometry *geo);
//static void setup_device(struct blkdev dev);

static void blkdev_transfer(struct blkdev *dev, sector_t sector, unsigned long nsect, char *buffer, int write)
{
	unsigned long offset = sector * Logical_blk_size;
	unsigned long size = nsect * Logical_blk_size;

	if((offset + size) > dev->size) {
		printk(KERN_INFO "\nstatus[%s]: Error in memory limitation\n", __func__);

		return;
	}
	
	if(write) {
		memcpy(dev->data + offset, buffer, size);
	} else {
		memcpy(buffer, dev->data + offset, size);
	}
}

static void blkdev_request(struct request_queue *que)
{
	struct request *req;

	req = blk_fetch_request(que);

	while(req != NULL) {
		/* Ignoring non fs / cmd request from the queue */
		if((req == NULL) || (req->cmd_type != REQ_TYPE_FS)) {
			printk(KERN_INFO "\nstatus[%s]: Skip non cmd request\n", __func__);
			__blk_end_request_all(req, -EIO);
			continue;
		}

		/* actual transfer by this function call */
		blkdev_transfer(&Dev, blk_rq_pos(req), blk_rq_cur_sectors(req), req->buffer, rq_data_dir(req));

		/* fetching the request from que based on this condition */
		if(! __blk_end_request_cur(req, 0)) {
			req = blk_fetch_request(que);
		}
	}
}

int blkdev_getgeo(struct block_device *block_device, struct hd_geometry *geo)
{
	long size;

	size = Dev.size * (Logical_blk_size / KERNEL_SECTOR_SIZE);
	geo->cylinders = (size & ~0x3f) >> 6;
	geo->heads = 4;
	geo->sectors = 16;
	geo->start = 0;
	
	return 0;
}

struct block_device_operations blkdev_fops = {
	.owner = THIS_MODULE,
	.getgeo = blkdev_getgeo,
};

#if 0
static void setup_device(struct blkdev dev)
{
	/* assigning the device size */
	dev.size = Logical_blk_size * Nsectors;
	spin_lock_init(&dev.lock);
	dev.data = vmalloc(dev.size);
	if(dev.data == NULL)
		return;

	printk(KERN_INFO "\nstatus[%s]: Success in assigning the device size\n", __func__);

	/* initializing request queue */
	dev.que = blk_init_queue(blkdev_request, &dev.lock);
	if(dev.que == NULL) {
		printk(KERN_INFO "\nstatus[%s]: Error in initializing the queue\n", __func__);

		goto out;
	}
	printk(KERN_INFO "\nstatus[%s]: Success in initializing the queue\n", __func__);
	blk_queue_logical_block_size(dev.que, Logical_blk_size);

	/* initializing the gendisk */
	dev.gd = alloc_disk(16);
	if(dev.gd == NULL) {
		goto out_unregister;
	}
	printk(KERN_INFO "\nstatus[%s]: Success in allocating the disk\n", __func__);
	dev.gd->major = Majorno;
	dev.gd->first_minor = Minorno;
	dev.gd->fops = &blkdev_fops;
	dev.gd->private_data = &dev;
	dev.gd->queue = dev.que;
	strcpy(dev.gd->disk_name, "blkdev0");
	set_capacity(dev.gd, 0);
	//add_disk(dev.gd);
	set_capacity(dev.gd, Nsectors);
	printk(KERN_INFO "\nstatus[%s]: Success in adding the disk\n", __func__);

out_unregister:
	unregister_blkdev(Majorno, "blkdev");	

out:
	vfree(dev.data);
}
#endif

static int __init blkdev_init(void)
{
	/* assigning the device size */
	Dev.size = Logical_blk_size * Nsectors;
	spin_lock_init(&Dev.lock);
	Dev.data = vmalloc(Dev.size);
	if(Dev.data == NULL)
		return -ENOMEM;

	printk(KERN_INFO "\nstatus[%s]: Success in assigning the device size\n", __func__);

	/* initializing request queue */
	Que = blk_init_queue(blkdev_request, &Dev.lock);
	if(Que == NULL) {
		printk(KERN_INFO "\nstatus[%s]: Error in initializing the queue\n", __func__);

		goto out;
	}
	printk(KERN_INFO "\nstatus[%s]: Success in initializing the queue\n", __func__);
	blk_queue_logical_block_size(Que, Logical_blk_size);

	/* Registering block device */
	Majorno = register_blkdev(Majorno, "blkdev");
	if(Majorno < 0) {
		printk(KERN_INFO "\nstatus[%s]: Error in registering blkdevice\n", __func__);
		
		return -EINVAL;
	}
	printk(KERN_INFO "\nstatus[%s]: Success in Registering blkdev - Majorno: %d\n", __func__, Majorno);

	/* initializing the gendisk */
	Dev.gd = alloc_disk(16);
	if(Dev.gd == NULL) {
		goto out_unregister;
	}
	printk(KERN_INFO "\nstatus[%s]: Success in allocating the disk\n", __func__);
	Dev.gd->major = Majorno;
	Dev.gd->first_minor = Minorno;
	Dev.gd->fops = &blkdev_fops;
	Dev.gd->private_data = &Dev;
	strcpy(Dev.gd->disk_name, "blkdev0");
	set_capacity(Dev.gd, Nsectors);
	Dev.gd->queue = Que;
	add_disk(Dev.gd);
	printk(KERN_INFO "\nstatus[%s]: Success in adding the disk\n", __func__);

//	setup_device(Dev);

	return 0;

out_unregister:
	unregister_blkdev(Majorno, "blkdev");	

out:
	vfree(Dev.data);
	return -ENOMEM;
}

static void __exit blkdev_exit(void)
{
	del_gendisk(Dev.gd);
	put_disk(Dev.gd);
	unregister_blkdev(Majorno, "blkdev");
	blk_cleanup_queue(Que);
	vfree(Dev.data);
}

module_init(blkdev_init);
module_exit(blkdev_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Stalin Srinivasan.S<stalinsrinivasan.subramanian@in.bosch.com>");
