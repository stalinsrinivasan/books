Introduction to Yocto:
=====================

Yocto project enables you to build custom linux based os for embedded products regardless of hardware architecture. Due to this, we can use one common linux for all major architectures. It provides flexible framework allows you to reuse your software stack with future devices. In final, Yocto provides us the tools and metadata to build our custom linux based OS image for different architectures.

Build System Overview:
======================

* Name of build system used by yocto is POKY which consists of two major components BitBake and Metadata

* BitBake - It is a task executor and scheduler (ie) which executes and manages all build steps

* metadata - It gives task definitions (ie) the info about what gets built and how to be built. and it is broken to 3 types

		- Configuration (.conf) -> global definition of variables (such as compile flags, path of build dir etc)
		- classes (.bbclass)    -> tells build logic (ie) how the software to be built
		- recipes (.bb)         -> Building blocks and logical units of software or images to build and tells what packages to be included

Finally, custom linux based images are created from repository of baked recipes (.bb) with proper (.conf) and (.bbclass).

Recipes are the instructions for building package such as where to obtain upstream sources, dependencies to be included, configuration / compilation option and define the files to go into output packages

Build system workflow:
=====================

	Input -> Build system -> output

Inputs / Metadata to build system:
---------------------------------

* User Configuration

* Metadata (.bb + patches)

* Mach BSP Configuration

* Policy Configuration

* source materials (upstream projects, local yocto projects and SCM (optional))


Build System:
------------

1. Fetching the sources (based on metadata input (.bb))

2. Patching the application (based on metadata input (patches))

3. configuration (based on user & Mach BSP Configuration)

4. Output Analysis of package for its splitting and relationship

5. Generation of packages (*.deb, *.rpm, *.ipk)

6. QA Tests

7. Image Generation & SDK Generation from output package feeds (gives pkg mgmt control over its installation order and dependencies)

Output:
------

* Images 

* Application Development SDK

Setup the yocto:
===============

prerequisites
-------------

	$ sudo apt-get install sed wget cvs subversion git-core coreutils unzip texi2html texinfo libsdl1.2-dev docbook-utils gawk python-pysqlite2 diffstat help2man make gcc build-essential g++ desktop-file-utils chrpath libgl1-mesa-dev libglu1-mesa-dev mercurial autoconf automake groff gcc-multilib xterm

Build setup:
------------

1. Git clone the poky from yoctoproject

	$ git clone http://git.yoctoproject.org/git/poky

2. setup the environment

	$ source oe-init-build-env <custom_build_folder>  // default: ./build

4. Change machine and cpu threads for build in ./build/conf/local.conf

	$ vim conf/local.conf
	/* change MACHINE=??qemux86 or MACHINE=??beaglebone */

3. building core-image

	$ bitbake core-image-minimal  <or>

	$ bitbake -k core-image-stato

5. run the image under emulation

	$ runqemu qemux86           <or>

	$ runqemu beaglebone

Recipe and its building:
========================

Example recipe name: ethtool_2.6.36.bb

Inside recipe:
-------------
SUMMARY = "Display / Configuring ethernet card settings"
DESCRIPTION = "utility for examining and tuning settings of ethernet based nw interface"
HOMEPAGE = "http://sourceforge.net/projects/gkernel/"
LICENSE ="GPLv2+"
SRC_URI ="${SOURCEFORGE_MIRRO}/gkernel/ethtool-${PV}.tar.gz"

inherit autotools // this tells bitbake to use bbclass files to build 

Standard Recipe Build Steps:
===========================

the following functions involves for building recipe and can be overwritten by us for customizations.

* do_fetch

* do_unpack

* do_patch

* do_configure

* do_compile

* do_install 

* do_package

Layers:
======

* Yocto project build system is composed of layers.

* Layer is a logical collection of recipes representing the core, bsp or application. 

* Each layer has its own priority and can override policy and config settings of layer which has low priority over that.

Layers Stack: (Bottom up approach)
============

* OpenEmbedded Core Metadata (oe-core) Layer --> come along with poky git repo or tar ball

* Yocto specific Metadata (meta-yocto) Layer --> come along with poky git repo or tar ball

* Hardware specific BSP Layer (machine specific layer TI (meta-ti), freescale etc)

* UI specific Layer (UI & Application libraries)

* Commercial Layer (from OSV) (commercial linux vendors like timesys, montavista , mentor graphics etc)

* Developer Specific Layer				

BSP Layer:
---------

* This layer to enable support for specific hardware platforms

* It defines machine configuration for the "board"

* It adds machine specific recipes and customizations such as kernel config, graphic drivers and additional recipes for hardware features.

Adding BSP layer to yocto:
==========================e
