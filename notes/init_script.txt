we can add our scripts as one of the startup script in linux

create your script - vi hello.sh

 #!/bin/sh
 <script body>

save the script

chmod a+x <script>

check the script functionality without errors or warnings

cp <script> /etc/init.d/.

ln -s /etc/init.d/<script> /etc/rc.d/rc<runlevel>.d/<S/K><id><script>

runlevel should be 0, 1, 2, 3, 4, 5

S50 at startup be in runlevel 2, 3, 4, 5

K50 at shutdown for cleanup be in runlevel 0, 1, 6

to get your current runlevel --> $ runlevel

it fetches the output from /var/run/utmp file

eg: runlevel 5, script filename hello.sh startup

------------------------------------------------------

* chmod a+x hello.sh

* cp hello.sh /etc/init.d/.

* ln -s /etc/init.d/hello.sh /etc/rc.d/rc5.d/S50hello.sh

---------------------------------------------------------

instead of creating symbolic link in /etc/rc<runlevel.d> for our script in /etc/init.d

just give the following command we can create symbolic links in runlevel directory

* update-rc.d <script> defaults

----------------------------------------

eg: update-rc.d hello.sh defaults

---------------------------------------

alternative: 

* update-rc.d -f <script> <start/stop> <order_of_script> <runlevel1> <runlevel2> <runlevel3> .

---------------------------------------------------

eg: update-rc.d -f hello.sh start 99 2 3 4 5 .        /* dont forget last '.' */

---------------------------------------------------

to remove script from your scritp from startup script list

---------------------------------------------------

eg: update-rc.d hello.sh remove

---------------------------------------------------
