#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <linux/ioctl.h>
#include <sys/mman.h>

#include "../drv/simple.h"

#define TEST_OPEN_CLOSE 1
#define TEST_READ_WRITE 2
#define TEST_IOCTL	3
//#define	TEST_IOCTL_ERRONEOUS	4

int main (void)
{
	int fd;
	char dummy;
	int ret;
#ifdef TEST_IOCTL
	int ioctl_int;
	unsigned long ioctl_ul;
	struct simple_ioctl ioctl_data;
#endif

#ifdef TEST_OPEN_CLOSE
	fd = open("/dev/simple", O_RDWR);
	if (0 > fd) {
		printf("\nDevice open failed: %s\n", strerror(errno));
		return -1;
	}
	printf("\nDevice Opened successfully, Descriptor: %d\n", fd);

#ifdef TEST_READ_WRITE	
	ret = read(fd, &dummy, 1);
	printf("\nRead returned %d\n", ret);

	ret = write(fd, &dummy, 1);
	printf("\nWrite returned %d\n", ret);
#endif

#ifdef TEST_IOCTL
	ret = ioctl(fd, SIMPLE_IOCTL_ONE, 0);
	if (0 > ret) {
		printf("\nIoctl (1) failed..: %s\n", strerror(errno));
		close (fd);
		return -1;
	}
	printf("\nIoctl (1) succeded..\n");

	ioctl_int = 0x87654321;
	ret = ioctl(fd, SIMPLE_IOCTL_TWO, &ioctl_int);
	if (0 > ret) {
		printf("\nIoctl (2) failed..: %s\n", strerror(errno));
		close (fd);
		return -1;
	}
	printf("\nIoctl (2) succeded..\n");

	ioctl_ul = 0xFF00FF00;
	ret = ioctl(fd, SIMPLE_IOCTL_THREE, &ioctl_ul);
	if (0 > ret) {
		printf("\nIoctl (3) failed..: %s\n", strerror(errno));
		close (fd);
		return -1;
	}
	printf("\nIoctl (3) succeded..\nData: 0x%lx\n", ioctl_ul);

	ioctl_data.arg1 = 0xFFFF0000;
	ioctl_data.arg2 = 0x0000FFFF;
	printf("\nData before ioctl (4) call: arg1: 0x%x, arg2: 0x%lx\n", ioctl_data.arg1, ioctl_data.arg2);
	ret = ioctl(fd, SIMPLE_IOCTL_FOUR, &ioctl_data);
	if (0 > ret) {
		printf("\nIoctl failed..: %s\n", strerror(errno));
		close (fd);
		return -1;
	}
	printf("\nData after ioctl (4) call: arg1: 0x%x, arg2: 0x%lx\n", ioctl_data.arg1, ioctl_data.arg2);
#ifdef TEST_IOCTL_ERRONEOUS
	ret = ioctl(fd, 10, ioctl_int);
	if (0 > ret) {
		printf("\nIoctl failed..: %s\n", strerror(errno));
		close (fd);
		return -1;
	}
#endif
#endif
	close(fd);
#endif
	return 0;
}

