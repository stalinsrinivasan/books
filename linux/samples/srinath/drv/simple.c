/*
 * Simple - simple char driver demonstration.
 *
 * The source code in this file can be freely used, adapted,
 * and redistributed in source or binary form, so long as an
 * acknowledgment appears in derived source files.  The citation
 * should list that the code comes from the book "Linux Device
 * Drivers" by Alessandro Rubini and Jonathan Corbet, published
 * by O'Reilly & Associates.   No warranty is attached;
 * we cannot take responsibility for errors or fitness for use.
 *
 */

#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>   /* printk() */
#include <linux/slab.h>   /* kmalloc() */
#include <linux/fs.h>       /* everything... */
#include <linux/errno.h>    /* error codes */
#include <linux/types.h>    /* size_t */
#include <linux/mm.h>		/* mmap */
#include <linux/kdev_t.h>
#include <linux/cdev.h>

#include <asm/uaccess.h>

#include "simple.h"

#define MODULE_NAME		"simple"

static int simple_major = 0;
module_param(simple_major, int, 0);

#define MAX_SIMPLE_DEV 2

static struct cdev simple_cdev;

/*
 * Common VMA ops.
 */

void simple_vma_open(struct vm_area_struct *vma)
{
	printk(KERN_NOTICE "%s: Simple VMA open, virt 0x%lx, phys 0x%lx\n", MODULE_NAME,
			vma->vm_start, vma->vm_pgoff << PAGE_SHIFT);
}

void simple_vma_close(struct vm_area_struct *vma)
{
	printk(KERN_NOTICE "%s: Simple VMA close.\n", MODULE_NAME);
}


/*
 * Open the device.
 */
static int simple_open (struct inode *inode, struct file *filp)
{
	printk(KERN_INFO "%s: device Opened.\n", MODULE_NAME);
	return 0;
}


/*
 * Closing is just as simpler.
 */
static int simple_release(struct inode *inode, struct file *filp)
{
	printk(KERN_INFO "%s: device Closed.\n", MODULE_NAME);
	return 0;
}

/*
 * The remap_pfn_range version of mmap borrowed
 * from drivers/char/mem.c.
 */

static struct vm_operations_struct simple_vm_ops = {
	.open =  simple_vma_open,
	.close = simple_vma_close,
};

static int simple_mmap(struct file *filp, struct vm_area_struct *vma)
{
	printk(KERN_INFO "%s: device Mmap.\n", MODULE_NAME);

	if (remap_pfn_range(vma, vma->vm_start, vma->vm_pgoff,
			    vma->vm_end - vma->vm_start,
			    vma->vm_page_prot))
		return -EAGAIN;

	vma->vm_ops = &simple_vm_ops;
	simple_vma_open(vma);
	return 0;
}

static int
simple_ioctl(struct inode *inode, struct file *filp, unsigned int cmd, unsigned long arg)
{
	switch (_IOC_NR(cmd)) {
	case 1:
		printk(KERN_INFO "%s: Ioctl Call with cmd: %d\n", MODULE_NAME, _IOC_NR(cmd));
		break;
	case 2:
		printk(KERN_INFO "%s: Ioctl Call with cmd: %d\n", MODULE_NAME, _IOC_NR(cmd));
		{
			int input;
			if (copy_from_user (&input, (void __user *)arg, _IOC_SIZE(cmd))) {
				printk(KERN_ERR "%s: copy_from_user failed..\n", MODULE_NAME);
				return -EFAULT;
			}
			printk(KERN_INFO "%s: User argument: int -> 0x%x\n", MODULE_NAME, input);
		}
		break;
	case 3:
		printk(KERN_INFO "%s: Ioctl Call with cmd: %d\n", MODULE_NAME, _IOC_NR(cmd));
		{
			unsigned long output = 0x12345678;
			if (copy_to_user ((void __user *)arg, &output, _IOC_SIZE(cmd))) {
				printk(KERN_ERR "%s: copy_to_user failed..\n", MODULE_NAME);
				return -EFAULT;
			}
		}
		break;
	case 4:
		printk(KERN_INFO "%s: Ioctl Call with cmd: %d", MODULE_NAME, _IOC_NR(cmd));
		{
			struct simple_ioctl data;
			if (copy_from_user (&data, (void __user *)arg, sizeof(struct simple_ioctl))) {
				printk(KERN_ERR "%s: copy_from_user failed..\n", MODULE_NAME);
				return -EFAULT;
			}
			printk(KERN_INFO "%s: Data from user -> arg1: 0x%x, arg2: 0x%lx\n", MODULE_NAME, data.arg1, data.arg2);
			data.arg1 = 0x12345678;
			data.arg2 = 0xAA55AA55;
			printk(KERN_INFO "%s: Data to user -> arg1: 0x%x, arg2: 0x%lx\n", MODULE_NAME, data.arg1, data.arg2);
			if (copy_to_user ((void __user *)arg, &data, sizeof(struct simple_ioctl))) {
				printk(KERN_ERR "%s: copy_to_user failed..\n", MODULE_NAME);
				return -EFAULT;
			}
		}
		break;
	default:
		printk(KERN_ERR "%s: Invalid Ioctl Call with cmd: %d\n", MODULE_NAME, _IOC_NR(cmd));
		return -EINVAL;
	}

	return 0;
}

static ssize_t simple_read(struct file * file, char __user * buf,
			size_t count, loff_t *ppos)
{
	printk(KERN_INFO "%s: Device Read..\n", MODULE_NAME);
	return count;
}

static ssize_t simple_write(struct file * file, const char __user * buf, 
			 size_t count, loff_t *ppos)
{
	printk(KERN_INFO "%s: Device Write..\n", MODULE_NAME);
	return count;
}

/*
 * File Operations.
 */
static struct file_operations simple_fops = {
	.owner   = THIS_MODULE,
	.open    = simple_open,
	.release = simple_release,
	.read	= simple_read,
	.write	= simple_write,
	.ioctl	= simple_ioctl,
	.mmap    = simple_mmap,
};

/*
 * Module housekeeping.
 */
static int simple_init(void)
{
	int result;
	dev_t dev = MKDEV(simple_major, 0);

	printk(KERN_INFO "%s: Driver Init.\n", MODULE_NAME);

	/* Figure out our device number. */
	if (simple_major)
		result = register_chrdev_region(dev, 1, MODULE_NAME);
	else {
		result = alloc_chrdev_region(&dev, 0, 1, MODULE_NAME);
		simple_major = MAJOR(dev);
	}
	if (result < 0) {
		printk(KERN_WARNING "%s: unable to get major %d\n", MODULE_NAME, simple_major);
		return result;
	}

	if (simple_major == 0)
		simple_major = result;

	/* Now set up the cdev */
	printk(KERN_INFO "%s: adding char device with major no.: %d, minor no.: 0\n", MODULE_NAME, simple_major);
	cdev_init(&simple_cdev, &simple_fops);
	simple_cdev.owner = THIS_MODULE;
	simple_cdev.ops = &simple_fops;
	result = cdev_add (&simple_cdev, MKDEV(simple_major, 0), 1);
	if (result) {
		printk (KERN_ERR "%s: Error %d adding simple\n", MODULE_NAME, result);
		unregister_chrdev_region(MKDEV(simple_major, 0), 1);
		return result;
	}

	return 0;
}

static void simple_cleanup(void)
{
	printk(KERN_INFO "%s: Driver Cleanup.\n", MODULE_NAME);
	cdev_del(&simple_cdev);
	unregister_chrdev_region(MKDEV(simple_major, 0), 1);
}


module_init(simple_init);
module_exit(simple_cleanup);

MODULE_AUTHOR("MISTRAL");
MODULE_LICENSE("GPL");

