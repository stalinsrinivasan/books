/*
 * Simple driver interface
 */
#ifndef _SIMPLE_H_
#define _SIMPLE_H_

#define SIMPLE_MAGIC_NO		'S'

/*
 * ioctl calls that are permitted to the /dev/simple driver
 */

#define SIMPLE_IOCTL_ONE	_IO(SIMPLE_MAGIC_NO, 0x01)
#define SIMPLE_IOCTL_TWO	_IOW(SIMPLE_MAGIC_NO, 0x02, int)
#define SIMPLE_IOCTL_THREE	_IOR(SIMPLE_MAGIC_NO, 0x03, unsigned long) /* Set alarm time  */
#define SIMPLE_IOCTL_FOUR	_IOWR(SIMPLE_MAGIC_NO, 0x04, struct simple_ioctl*) /* Set alarm time  */

#define SIMPLE_MAX_IOCTL	4

struct simple_ioctl {
	int arg1;
	unsigned long arg2;	
};

#endif /* _SIMPLE_H_ */
