#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/io.h>

#define MODULE_NAME "simple_driver"
#define INC_VALUE 5

int major_num ;
char drv_buffer[4];
int temp_buffer;

int drv_open(struct inode *inode, struct file *fptr);
int drv_release( struct inode *inode, struct file *fptr );
ssize_t drv_read( struct file *fptr, char __user* buffer, size_t size ,loff_t *fpos );
ssize_t drv_write( struct file *fptr, const char __user* buffer, size_t size ,loff_t *fpos );
static int drv_init( void );
static void drv_cleanup( void );

static struct file_operations drv_fops = { 
	.owner = THIS_MODULE,
	.open = drv_open,
	.release = drv_release,
	.read = drv_read,
	.write = drv_write,
};

static int drv_init( void )
{
	int return_value ;
	major_num = 0;

	return_value = register_chrdev( major_num, MODULE_NAME, &drv_fops );
	if( return_value < 0 )
	{
		printk( KERN_WARNING "%s:Error in registering\n", MODULE_NAME );
		return return_value ;
	}
 
	major_num = return_value;

	printk( KERN_INFO "\nInitialized driver %s with major number %d\n", MODULE_NAME , major_num  );
	return 0;
}

static void drv_cleanup( void )
{
	unregister_chrdev( major_num , MODULE_NAME );

	printk( KERN_INFO "%s:Driver cleaned up.\n", MODULE_NAME );
}

int drv_open( struct inode *inode, struct file *fptr )
{
	printk( KERN_INFO "%s:Opening driver. \n", MODULE_NAME );
	return 0;
}

int drv_release( struct inode *inode, struct file *fptr )
{
	printk( KERN_INFO "%s:Releasing driver. \n", MODULE_NAME );
 	return 0;
}

ssize_t drv_read(struct file *fptr, char __user* buffer, size_t size ,loff_t*fpos )
{
	temp_buffer = simple_strtol( drv_buffer, NULL, 10 );
	temp_buffer = temp_buffer + 5 ;

	printk( KERN_INFO "%s:Variable copied to user:%d\n", MODULE_NAME, temp_buffer );

	copy_to_user( buffer, &temp_buffer, sizeof(temp_buffer) );

	return size;
}

ssize_t drv_write( struct file *fptr, const char __user* buffer, size_t size, loff_t *fpos )
{
	copy_from_user( drv_buffer, buffer, sizeof(buffer) );

	return size;
}

module_init( drv_init );
module_exit( drv_cleanup );


MODULE_LICENSE("GPL");
MODULE_AUTHOR("SABIYA");
MODULE_DESCRIPTION("SIMPLE DRIVER IMPLEMENTATION");

