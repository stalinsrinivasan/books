#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/io.h>

#define MODULE_NAME "simple_driver"
#define INC_VALUE 5

int major_num ;
char drv_buffer[4];
int temp_buffer;

int drv_open(struct inode *inode, struct file *fptr);
int drv_release( struct inode *inode, struct file *fptr );
ssize_t drv_read( struct file *fptr, char __user* buffer, size_t size ,loff_t *fpos );
ssize_t drv_write( struct file *fptr, const char __user* buffer, size_t size ,loff_t *fpos );
static int drv_init( void );
static void drv_cleanup( void );



