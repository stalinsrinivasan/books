#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

int main( void )
{
	int fd, choice, out ,flag, count ;
	char *in ;

	in = malloc(sizeof(char*));
	memset( in, 0, 4 ) ;

	fd= open( "/dev/simple_driver", O_RDWR ) ;

	if( fd < 0 )
	{
		printf( "\nError opening driver%d\n", fd ) ;
		perror( "open:" ) ;
		exit( 0 ) ;
	}
	else
	{
		while( 1 )
		{
			choice = 0 ;	
			flag = 0;
			count = 0;

			printf( "\nEnter ur choice\n1.read\n2.write\n3.exit\n" ) ;
			scanf( "%d", &choice ) ;

			if( choice != 1 && choice!= 2 && choice!= 3 )
			{
				printf( "\nEnter proper choice\n" ) ;
				break ;
			}
			else
			{
				switch( choice )
				{
					case 1 :
						read( fd, &out, 4 ) ;
						if( out == 5 )
							printf("\nNo data\n");
						else
							printf( "\nData retrieved is %d\n", out ) ;
						//llseek( fd , 3 );
						//perror("err:");
						break ;
					case 2 :
						printf( "\nEnter the input(only 4 bytes)\n" ) ;
						scanf( "%s", in ) ;
						while( *in != '\0' )
						{
							if((isalpha( *in )) || (ispunct( *in )))
							{
								flag++;
							}
							count++;
							in++;
						}
						if( flag > 0 )
								printf("\nEnter only number\n");
						else
						{
							if( count > 4 )
								printf("\nEnter 4 bytes only\nData not stored\n");	
							else
							{
								in = in - count ;
								write( fd, in, 4 ) ;
							}
						}
						break;
					case 3 :
						close( fd ) ;
						exit( 0 ) ;
				}
			}
		}
	}
	return 0;
}








