#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/kernel.h>
#include <linux/slab.h>
#include <linux/fs.h>
#include <linux/types.h>
#include <linux/errno.h>
#include <asm/uaccess.h>
#include <linux/io.h>

#define MAX_SIZE 		20
#define SIMPLE_IOCTL_ONE 	1
#define MODULE_NAME 		"simple_driver"

int driver_open (struct inode *inode, struct file *fptr);
int driver_release (struct inode *inode, struct file *fptr);
ssize_t driver_read (struct file *fptr, char __user *user_buffer, size_t size, loff_t *offset);
ssize_t driver_write (struct file *fptr, const char __user *user_buffer, size_t size, loff_t *offset);
static int driver_ioctl (struct inode *inode, struct file *fptr, unsigned int cmd, unsigned long arg);
static int driver_init (void);
static void driver_exit (void);

static struct file_operations driver_fops = { 
	.owner = THIS_MODULE,
	.open = driver_open,
	.release = driver_release,
	.read = driver_read,
	.write = driver_write,
	.ioctl = driver_ioctl,
};

static int major_number = 0;
static DECLARE_WAIT_QUEUE_HEAD (my_queue);
static int flag = 0;

static int driver_init (void)
{
	int result;
	
	result = register_chrdev (major_number , MODULE_NAME , &driver_fops);
	if (result < 0)
	{
		printk (KERN_WARNING "%s:Error in registering\n", MODULE_NAME);
		return result ;
	}
	major_number = result;
	printk (KERN_INFO "\nInitialized driver %s with major_number number %d and minor number 0\n", MODULE_NAME , major_number);
	
	return 0;
}

static void driver_exit (void)
{
	if (major_number != 0)
	{
		unregister_chrdev (major_number , MODULE_NAME);

		printk (KERN_INFO "%s:Driver cleaned up.\n", MODULE_NAME);
	}
	return;
}

int driver_open (struct inode *inode , struct file *fptr)
{
	if ((fptr->private_data = (char *) kmalloc (MAX_SIZE * sizeof(char), GFP_KERNEL)) == NULL)
	{
		printk (KERN_WARNING "%s:Error in allocation\n", MODULE_NAME);
		return -EFAULT;
	}
	memset (fptr->private_data, '\0', MAX_SIZE);
	printk (KERN_INFO "%s:Opening driver.\n", MODULE_NAME);
	return 0;
}

int driver_release (struct inode *inode , struct file *fptr)
{
	if (fptr->private_data)
		kfree (fptr->private_data);
	
	printk (KERN_INFO "%s:Releasing driver.\n", MODULE_NAME);
 	return 0;
}

ssize_t driver_read (struct file *fptr , char __user *user_buffer , size_t size , loff_t *offset)
{
	wait_event_interruptible (my_queue, flag != 0);
	flag = 0;

	printk (KERN_NOTICE "Simple-driver: Device file is read at offset = %i, read bytes count = %u"
				, (int)*offset
				, (unsigned int)size);
	if (*offset >= MAX_SIZE)
		return 0;

	if (*offset + size > MAX_SIZE)
		size = MAX_SIZE - *offset;

	user_buffer[size] = '\0';
	if (copy_to_user (user_buffer, (fptr->private_data) + *offset , size) != 0 )
		return -EFAULT;	
	
	*offset += size;
	return size;
}

ssize_t driver_write (struct file *fptr , const char __user *user_buffer , size_t size , loff_t *offset)
{
	 flag = 1;
	 wake_up_interruptible (&my_queue);

	printk (KERN_NOTICE "Simple-driver: Device file is written at offset = %i, written bytes count = %u"
				, (int)*offset
				, (unsigned int)size);
	if (*offset >= MAX_SIZE)
		return 0;

	if (*offset + size > MAX_SIZE)
		size = MAX_SIZE - *offset;
	
		
	if (copy_from_user ((fptr->private_data) + *offset , user_buffer  , size) != 0 )
		return -EFAULT;	

	*offset += (size - 1);
	return size;
}

static int driver_ioctl (struct inode *inode , struct file *fptr , unsigned int cmd , unsigned long arg)
{
	int length = 0;
	char *p = (fptr->private_data);
	
	switch (_IOC_NR(cmd)) 
	{
	case SIMPLE_IOCTL_ONE:
		printk (KERN_INFO "%s: Ioctl Call with cmd: %d\n", MODULE_NAME, _IOC_NR(cmd));
		
		while (*p != '\0')
		{
			p++;
			length++;
		}
		
		if (copy_to_user ((int *)arg , &length , _IOC_SIZE(cmd)) != 0 )
			return -EFAULT;	
		break;
	
	default:
		printk(KERN_ERR "%s: Invalid Ioctl Call with cmd: %d\n", MODULE_NAME, _IOC_NR(cmd));
		return -EINVAL;
	}

	return 0;
}

module_init (driver_init);
module_exit (driver_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("SANJAY");
MODULE_DESCRIPTION("SIMPLE DRIVER IMPLEMENTATION");

