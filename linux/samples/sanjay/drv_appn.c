#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <linux/errno.h>
#include <linux/ioctl.h>
#include <errno.h>

#define SIMPLE_MAGIC_NO		'~'

#define SIMPLE_IOCTL_ONE	_IOR(SIMPLE_MAGIC_NO, 0x01, unsigned long)

int main (void)
{
	int fd, choice = 0 , string_length = 0 , len;
	char to_user[20];
	char from_user[15];
	
	fd = open ("/dev/simple_driver", O_RDWR);

	if (fd < 0)
	{
		printf ("\nError opening driver%d\n", fd);
		perror ("open:");
		exit (0);
	}
	else
	{
		while (1)
		{	
			printf ("\nEnter your choice\n1.read\n2.write\n3.strlen\n4.exit\n");
			scanf("%d",&choice);

			switch (choice)
			{
				case 1 :  lseek (fd , 0 , SEEK_SET);
					  if ((read (fd , to_user , 5)) != 0)
						printf("data read: %s\n",to_user);
					
					  else
					  	printf("no data to read\n");
					  
					  if ((read (fd , to_user , 5)) != 0)
						printf("data read: %s\n",to_user);
					
					  else
					  	printf("no data to read\n");
					  break;
				
				case 2 :  printf ("enter a string to write (less than 15 characters)\n");
					  scanf ("%s", from_user);
					  len = strlen (from_user);
					  
					  //lseek (fd , 0 , SEEK_SET);
					  
					  if ((write (fd , from_user , len + 1)) < 0)
					  {
					  	printf ("\nwrite failed..: %s\n", strerror(errno));
						close (fd);
						return -1;
					  }
					  break;
				
				case 3 :  if ((ioctl (fd, SIMPLE_IOCTL_ONE , &string_length)) < 0)
					  {
					  	printf ("\nIoctl (1) failed..: %s\n", strerror(errno));
						close (fd);
						return -1;
					  }
					  
					  printf("string length = %d\n",string_length);
					  break;
				
				case 4 :  
					  close (fd);
					  exit (0);
				
				default : printf("enter correct choice\n");
					  break;
			}
			strcpy (to_user," ");
		}
	}
	return 0;
}

