#include <stdio.h> 
#include <string.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <signal.h> 

sigset_t s1,s2;

struct sigaction act1,act2;


void sig_hdl1(int signo)
{
  printf("a dummy signal handler1\n");
  //exit(0); 

}  
void sig_hdl2(int signo)
{
  printf("a dummy signal handler2\n");
  kill(getpid(),SIGKILL); 

}

int main(int argc, char *argv[]) 
 { 

     int ret;
     sigset_t set1,set2;

     

     if (argc != 2) 
         exit(0); 
 
     sigfillset(&set1);
    // sigdelset(&set1, SIGTERM); 
     sigprocmask(SIG_SETMASK, &set1,&set2) ;
  
     sigdelset(&set1, SIGQUIT);
     sigprocmask(SIG_SETMASK,&set1, &set2); 

     printf("we are blocked in the first sigsuspend\n"); 

     //sigsuspend(&set1); //block until an unmasked signal arrives

   

     //the bit corresponding to SIGTERM is reset in the user-space
     //bit-map set1 only
     //does not affect the signal mask field in the process descriptor
     sigdelset(&set1, SIGTERM); 
     sigdelset(&set1, SIGINT); 
     

     size_t mb = strtoul(argv[1], NULL, 0); 
 
     // Allocate the memory 
     size_t nbytes = mb * 0x100000;

     printf("the actual value is %lu\n", nbytes); 
 
     char *ptr = (char *) malloc(nbytes); 
     if (ptr == NULL) { 
         perror("malloc"); 
         exit(EXIT_FAILURE); 
     } 

     act1.sa_handler = sig_hdl1;
     act1.sa_flags = 0;  //currently, we ignore the flags
     sigfillset(&act1.sa_mask);

     //act2 is the parameter used to back-up the previous action
     //for this signal in this process
     //act1 is the parameter used to set the new action for this
     //process, as mentioned in the fields of act1
     sigaction(SIGTERM,&act1,&act2); 

     act1.sa_handler = sig_hdl2;
     act1.sa_flags = 0;  //currently, we ignore the flags
     sigfillset(&act1.sa_mask);
     sigaction(SIGINT,&act1,&act2); 
 

    //sigsuspend() blocks until an unmasked signal in the mask
     //passed to it arrives - while the sigsuspend() is blocked,
     //the signalmask used by the process is the one that is
     //passed to sigsuspend, not its original signal mask
     //when sigsuspend() wakes-up, the process starts using 
     //its original signalmask field  
     ret = sigsuspend(&set1);//this process normally blocks here 

     //if a handler is installed, sigsuspend() will return with
     //a -1 in ret and errno set to EINTR



     printf("the starting address of the mem. is %x\n", (unsigned int)ptr); 
     // Touch the memory (could also use calloc()) 
     size_t i; 
     const size_t stride = sysconf(_SC_PAGE_SIZE); 
     for (i = 0; i < nbytes; i += stride) { 
       ptr[i] = 0; 
     } 

     sigfillset(&set1);
     sigprocmask(SIG_SETMASK, &set1,NULL) ; 
 
     printf("allocated %d mb\n", mb); 
     
     //DO NOT USE pause() - use the newer sigsuspend()
     //pause();   //this is a system call that will block the process temporarily
                //until a signal arrives
                //if a signal arrives and there is no signal handler installed,
                //the process will wake-up and the default action will be taken
                //if a signal handler is installed, the process will be woken-up
                //,the handler will execute and the process will continue after 
                //pause()
                

     //sigsuspend() suspends(blocks) the current process and also, temporarily
     //replaces the process signal mask with a new mask; if a signal that is
     //not blocked by the temp. mask arrives, the process is woken up and action
     //is taken - the action may be a default or a signal handler may execute and
     //the process may resume after sigsuspend() 
     
//
     while(1){
     sigdelset(&set1, SIGTERM);
     ret = sigsuspend(&set1);
     }  
     


     return 0; 










}
