#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include<stdio.h>
#include<stdlib.h>


int main()
{

   int ret;

   //since the parent process and the child process execute the same
   //piece of code(program), the child will also be having fork() in
   //its code - but, the fork() will not execute fully, but partially
   //and return 0 when the child process is scheduled sometime in the
   //future 
   //
   //
   ret = fork();
 
   if(ret<0){ perror("error in fork"); exit(1); }

   //the return value ret is a +ve no. in the parent process
   //  
   if(ret>0){ 
	   

	   while(1){ 

                 printf("P\n");
	   printf("I am in parent process context\n"); 
           printf("in parent .. ppid is %d ...and pid is %d\n", 
		   getppid(),getpid());	   

           }
	   
	   
	   exit(0); 
   }

   if(ret==0) { 
	   

 
	   while(1){ 

               printf("C\n");


	   printf("I am in child process context\n"); 
           printf("in child .. ppid is %d ...and pid is %d\n", 
		   getppid(),getpid());	   

           }
	   exit(0); 
   }

return 0;
   
}


