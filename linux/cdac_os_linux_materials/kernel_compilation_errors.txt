1. the kernel is compiled and system is rebooted, but 
   user-space system call test application prints the following:

   - error in test call : function not implemented 

2. if you see the above error, it means, the current kernel 
   image that is loaded does not have our system call system routine
   implemented !!!

3. following are verifications that you may have to do:
   while(1)    
   {
   - uname -r  (to verfiy the kernel's version )

   - uname -v  (to verify kernel's date/time of build/compilation) 

   - if above information is upto date, do the following:
    
      - check you changes in the kernel source directory !!
      - in kernel source directory, run "make   kernel/sys.o" 
      - the above command will check whether there are errors
        in your source code - this can save time !!!
      - make -j <n>  //n = 2 * no.of cores        
      - one of the common errors seen are errors in 
        header(s) - in most cases, the headers were modified
        wrongly !!!
      - when you see the errors, look into the line no.s which 
        contain the errors !!
      - if you have not seen any errors so far, all that has to 
        be done is kernel image recompilation !!!
      - reboot again 
 
     }

4. when to use, which kernel - when to use MP support and
   when to use UP support !!!
   - whenever you are compiling/building kernel image, 
     you must be in MP support - this speeds up the build process
   - always verify whether all cores enabled - run 
     "cat /proc/cpuinfo" command !!!
   - whenever you are testing any system call API or any program 
     that uses system call APIs, us UP environment for initial
     testing/learning - after you have verified that your program
     /application works properly in UP mode, you can test it in 
     MP mode !!!
   - in your system, your development kernel typically works in 
     UP processor mode - if needed, your development kernel can 
     be forced to execute in MP processor mode also !!!
   - your standard kernel will always be configured to work 
     in MP processor mode - even your standard kernel may 
     be forced to work in UP mode !!!
        



 









