
asmlinkage long sys_test_call(unsigned int *pid, 
                                        unsigned int *tgid)

{   
        unsigned int pid_l,tgid_l;
        int ret;

        pid_l  = current->pid;
        tgid_l = current->tgid;

        printk("we have entered the syscall routine\n"); 

        ret = copy_to_user(pid,&pid_l,sizeof(pid_l));  
        if(ret>0) { printk("error1 %d\n",`EFAULT); return -EFAULT;}
              
        ret = copy_to_user(tgid,&tgid_l,sizeof(tgid_l));                
        if(ret>0) { printk("error2\n"); return -EFAULT;}

        return 0; 


} 





