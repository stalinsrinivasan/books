#include<errno.h>
#include<asm/unistd.h>
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include "syscallmacros.h"
#define __NR_test_call 337 
extern char **environ;

_syscall3(ssize_t,write,int,fd,const void *,buf,size_t,count)

_syscall3(long,open,const char *,filename,int,flags,int,mode)

_syscall0(pid_t,getpid)
_syscall0(pid_t,fork)

_syscall2(long,test_call,unsigned int*,pid,unsigned int *,tgid)
int main()
{
    

   int ret,ret1;

   unsigned int pid,tgid;

   char str[1024];

   //ret = fork();  //we are invoking fork system call from a process/program



   ret = snprintf(str,sizeof(str), "the pid of the process is %d\n", getpid());
  
   ret1 = write(STDOUT_FILENO,str,ret+1);

   if(ret1<0){ perror("error in write syscall"); exit(1);}

   ret1 = getpid();
   

   if(ret1<0){ perror("error in open syscall"); exit(2);}


   ret1 = test_call(&pid,&tgid);
   if(ret1<0) { perror("error in test_call"); exit(3); }
                //normal termination with errors 

   exit(0); //normal termination without any errors 
}
              


