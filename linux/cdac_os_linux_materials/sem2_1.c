#include<sys/ipc.h>
#include<sys/sem.h>
#include<sys/types.h>
#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>

#define KEY1 1234

union semun {
             int val;                  /* value for SETVAL */
             struct semid_ds *buf;     /* buffer for IPC_STAT, IPC_SET */
             unsigned short *array;    /* array for GETALL, SETALL */
                                       /* Linux specific part: */
             struct seminfo *__buf;    /* buffer for IPC_INFO */
       };


int main()

  {

    int ret1, ret2, id1, id2;
    union semun u1;
    struct sembuf sb1,sboa[5];
    unsigned short ary1[3],ary[3] = { 50,1,0};


    //here, we are creating a semaphore obj. with a single semaphore 

    id1 = semget(KEY1, 1, 0);
    if(id1<0) { perror("error in semget"); exit(1); }

    //semctl is a system call to initialize semaphore(s) in a     
    //semaphore object 
    //

    //we are initializing the first semaphore value to 1
    u1.val = 0;
    semctl(id1,0,SETVAL,u1); 

    ret1 = semctl(id1,0,GETVAL); 
    printf("the value of sem 0 is %lu\n",ret1);

    //here we are operating on the semaphore using decrement 
    //operation 

    sboa[0].sem_num = 0;
    sboa[0].sem_op = +1;
    sboa[0].sem_flg = 0;

    semop(id1, sboa, 1);
    
    ret1 = semctl(id1,0,GETVAL); 
    if(ret1<0){ perror("error in getting the value"); exit(1); } 
    printf("the value of sem 0 is %lu\n",ret1);
    

    //here we are destroying the entire semaphore object 
    //and all semaphores are destroyed
    //the second parameter can have any value - it is actually unused 

    //semctl(id1,0,IPC_RMID); //to delete or destroy the 
                            // semaphore object and all
                            // all semaphores in it 


}

