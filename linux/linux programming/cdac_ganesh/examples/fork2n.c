#include<sys/types.h>
#include<unistd.h>
#include<sys/wait.h>
#include<stdio.h>
#include<stdlib.h>


int main()
{

   int ret,status;

   unsigned long int i=0;

  while(1){

   ret = fork();
 
   if(ret<0){ 
           perror("error in fork"); 
           printf("the final value of i is %lu\n", i);
          
           exit(1); 
   }

   if(ret>0){ 
	   printf("I am in parent process context\n"); 
           printf("in parent .. ppid is %lu ...and pid is %lu\n", 
		   getppid(),getpid());	   
	   
           //this is the synch. point

	   //while(1){ printf("P\n");}
	   
	   //wait(NULL);
	   
	   //pause();
          // ret = waitpid(-1,&status,0);
           continue;
   }

   if(ret==0) { 
	   printf("I am in child process context\n"); 
           printf("in child .. ppid is %lu ...and pid is %lu\n", 
		   getppid(),getpid());	   
	   

           //do any work in the child process
           ++i;
	   exit(0); 
   }

 }//while

 if(ret>0)
 {
   while(1){ 
    ret = waitpid(-1,&status,0);
    if(ret>0){

    if(WIFEXITED(status))     //normal termination of the process
    {
       if(WEXITSTATUS(status) == 0){ //normal and successfull
       }
       else{//normal, but not successfull

       }
    }
    else{ //abnormal (did not succeed)
    }
   }//ret>0

    if(ret<0) { exit(0); } //no child is any state for this process
                           //all the children have terminated and 
                           //cleaned-up by the parent process
   } //second while 
  }//if after while loop 

return 0;
   
}


