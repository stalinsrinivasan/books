/*	FileName	:	p3.c
 *	Date		:	24-08-2010
 *	Author		: 	Stalin Srinivasan.S
 *	Description	:	to get clear about execl() when it called by process which is overwritten by its own address space
 */

#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>

int main()
{
	int i=1; /* values are inherited to parent process and not modification in child process not reflects in parent */
	if(fork())
	{	
		wait();
		i=2;
		if(fork())
		{
			wait();
			i=3;
			if(fork())
			{
				wait();
				printf("value of i: %d\n",i);	/* executes only in parent process */

				return 0;
			}
		}
	}

	printf("value of i: %d\n",i); /* displays the value in all 3 child processes */	

	i=5; /* modification in child process */

	exit(0); /* exit for child processes */
}
				
