/*	FileName	:	exc.c
 *	Date		: 	24-08-2010
 *	Author		:	Stalin Srinivasan.S
 *	Description	:	To get clear picture about execl() system call
 */

#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main(void)
{
	int a,b,c;

	if(fork()==0) /* in first child process */
	{
		/* first child process compiles another process to be execl and create exe */

		execl("/usr/bin/gcc","gcc","-o","multiplication","multiplication.c",NULL);

		exit(0); /* first child process terminates */
	}

	wait(); /* parent process wait till child process terminates */

	if(fork()==0) /* after termination of first child process, parent forks new second child process */
	{
                 /* second child process running another process */
	
		execl("multiplication",NULL,NULL);

	        /* these lines wont get executed since process terminates from that process(multiplication itself) 
                 * its address space has been overwritten by that process */

		printf("\n********* first try for addition **************\n");
		printf("\nEnter 2 nos: ");
		scanf("%d\t%d",&a,&b);
		c=a+b;
		printf("\nResult: %d",c);
		exit(0);
	}

	system("rm multiplication");
	
	wait(); /* parent process till second child process terminates */
    
        /* since parent process not overwritten by another 
         * process in execl this below code will run */

	printf("\n********* second try for addition **************\n");
	printf("\nEnter a value: ");
	scanf("%d", &a);
	printf("\nEnter b value: ");
	scanf("%d", &b);
	c=a+b;
	printf("\nResult: %d\n\n",c);

	return 0;
}
