/*	FileName	: p3c.c
 *	Date		: 24-08-2010
 *	Author		: Stalin Srinivasan.S
 *	Description	: To get into parent and child relationship in case of child termination
 */	
	
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
int main()
{
	if(fork()) /* first child process created */
	{	
		wait(); /* allow the first child process executes and terminates */

		if(fork()) /* second child process created */
		{
			wait(); /* allows the second child process executes and terminates */

			if(fork()) /*third child process created */
			{
				wait(); /* allows the third child process executes and terminates */

				printf("Boss.. I have catched. How is it?\n");

				exit(3); /* parent exit from here only, verify echo$? */
			}
		}
	}
	printf("\n I am child and now i exits");
	exit(0);
}
				
