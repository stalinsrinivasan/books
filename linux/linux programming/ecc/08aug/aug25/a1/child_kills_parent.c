/*	FileName	:	test.c
 *	Date		: 	25-08-2010
 *	Author		:	Stalin Srinivasan.S		
 *	Description	:	To signal from one process to other 
 */

#include<stdio.h>
#include<stdlib.h>
#include<signal.h>

int main(void)
{
	int i=0;
	for(i=0;i<1000;i++)
	{
		if(i==100)
		{
			if(fork())
			{
				printf("\nparent and i forked one child\n");
			}
			else
			{
				kill(getppid(),SIGQUIT);
				printf("\n i quitted my parent\n");
				exit(0);
			}
		}
		else 
		{
			printf("\nparent process i value: %d",i);
		}
	}
}
	


