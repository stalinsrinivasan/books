<HTML>

<HEAD>

<TITLE>Linux System Administrator's Survival Guide lsg06.htm </TITLE>

<LINK REL="ToC" HREF="index.htm">

<LINK REL="Index" HREF="htindex.htm">

<LINK REL="Next" HREF="lsg07.htm">

<LINK REL="Previous" HREF="lsgpt02.htm"></HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000" LINK="#0000FF" VLINK="#800080">

<A NAME="I0"></A>

<H2>Linux System Administrator's Survival Guide lsg06.htm</H2>

<P ALIGN=LEFT>

<A HREF="lsgpt02.htm" TARGET="_self"><IMG SRC="purprev.gif" WIDTH = 32 HEIGHT = 32 BORDER = 0 ALT="Previous Page"></A>

<A HREF="index.htm" TARGET="_self"><IMG SRC="purtoc.gif" WIDTH = 32 HEIGHT = 32 BORDER = 0 ALT="TOC"></A>

<A HREF="lsg07.htm" TARGET="_self"><IMG SRC="purnext.gif" WIDTH = 32 HEIGHT = 32 BORDER = 0 ALT="Next Page"></A>


<HR ALIGN=CENTER>

<P>

<UL>

<UL>

<UL>

<LI>

<A HREF="#E68E35" >Device Drivers</A>

<LI>

<A HREF="#E68E36" >Character and Block Mode Devices</A>

<UL>

<LI>

<A HREF="#E69E69" >Major and Minor Device Numbers</A>

<LI>

<A HREF="#E69E70" >The mknod Command</A></UL>

<LI>

<A HREF="#E68E37" >Device Permissions and Links</A>

<LI>

<A HREF="#E68E38" >Summary</A></UL></UL></UL>

<HR ALIGN=CENTER>

<A NAME="E66E8"></A>

<H1 ALIGN=CENTER>

<CENTER>

<FONT SIZE=6 COLOR="#FF0000"><B>Chapter 6</B></FONT></CENTER></H1>

<BR>

<A NAME="E67E11"></A>

<H2 ALIGN=CENTER>

<CENTER>

<FONT SIZE=6 COLOR="#FF0000"><B>Devices and Device Drivers</B></FONT></CENTER></H2>

<BR>

<P>One of the primary concepts of Linux that you must understand to administer a Linux system properly is that of devices. Without a basic knowledge of devices and device drivers, you can't add new hardware, manage existing hardware, or change your Linux hardware configuration. Luckily, devices and device drivers are very easy to understand.

<BR>

<P>This short chapter is devoted to devices and device drivers. In this chapter, you learn what a device driver is, how to handle the two different types of devices (character and block mode), what the major and minor device numbers are, and how to use the mknod command. Once you understand these concepts, you can change, add to, and manage your Linux hardware easily. Devices are referred to throughout this section of the book, so you should understand these concepts before going on.

<BR>

<BR>

<A NAME="E68E35"></A>

<H3 ALIGN=CENTER>

<CENTER>

<FONT SIZE=5 COLOR="#FF0000"><B>Device Drivers</B></FONT></CENTER></H3>

<BR>

<P>The Linux operating system has no built-in instructions for handling hard drives, floppy disk drives, keyboards, monitors, or any other peripheral or piece of hardware attached to the system. All the instructions for communicating with the peripheral are contained in a file called a device driver. Device drivers are usually a small piece of assembler or C code that have a set of instructions for sending and receiving data with the device.

<BR>

<P>You make the device driver a part of the Linux kernel by linking it. Linking means that the code becomes part of the operating system kernel and is loaded automatically when Linux boots, which allows Linux to communicate with the device much faster than if it had to read the instructions from the device driver every time a request to communicate with the device was issued. Linux can have many different device drivers linked to it; the number is limited only by the amount of RAM in your system. Practically, though, the number of device drivers is kept to a few dozen because most systems don't have too many devices attached.

<BR>

<P>When an application instructs a device to perform some action, the Linux kernel doesn't have to worry about the mechanism to perform the act. It simply passes the request to the device driver and lets it handle the communications. Similarly, when you are typing at the keyboard, your terminal's device driver accepts the keystrokes and passes them to the shell, filtering out any special codes that the kernel doesn't know how to handle by translating them into something the kernel can perform.

<BR>

<P>You can use a single device driver to instruct Linux about communicating with many different devices, as long as they use the same basic instructions. For example, if you have four terminals attached to your Linux system but they all use the same serial communications method, a single device driver linked into the kernel can handle all four terminals. Linking new device drivers into the kernel is dealt with later in this book in <A HREF="lsg25.htm">Chapter 25</A>, &quot;Modifying the Kernel.&quot;

<BR>

<P>The use of device drivers is one of the real strengths of the UNIX operating system (and therefore Linux too) because it provides a continual method for expanding the system using the same kernel and adapting existing systems to new devices as they are developed. When a new hardware device is developed, it can be used with Linux by programming a device driver, which is usually not a difficult task for an experienced programmer.

<BR>

<P>Linux keeps all device drivers in the /dev directory by default and convention. Figure 6.1 shows part of a typical Linux /dev directory listing. You can't see all the device drivers on a single screen because there are so many of them. Over 500 device drivers are included with the Slackware Linux system on the CD-ROM supplied with this book. Many are simply duplicates of a basic device driver with a different device number, but each file has to be separate.

<BR>

<P><B> <A HREF="06lsg01.gif">Figure 6.1.</B>

<BR><B>A partial listing of the /dev directory.</A></B>

<BR>

<P>You can keep device drivers anywhere on the Linux filesystem, but keeping them all in /dev makes it obvious that they are device drivers. If you check the /dev directory, you see that many of the files are actually links to other files. These links enable you to name devices more clearly. You can access the CD-ROM drive as /dev/cdrom instead of /dev/sbpcd2, for example. Likewise, the mouse device can be linked to /dev/mouse. These links are a convenience for the user and are not necessarily important for Linux. Figure 6.2. shows a long listing of some of the device drivers. You can see that the file cdrom is linked to the file /dev/scd0. <A HREF="lsg18.htm">Chapter 18</A>, &quot;Filesystems and Disks,&quot; discusses links and what they mean.

<BR>

<P><B> <A HREF="06lsg02.gif">Figure 6.2.</B>

<BR><B>A long directory listing of the /dev directory shows that some of the </B><B>files are links to other files, as both cdrom and core are here.</A></B>

<BR>

<BR>

<A NAME="E68E36"></A>

<H3 ALIGN=CENTER>

<CENTER>

<FONT SIZE=5 COLOR="#FF0000"><B>Character and Block Mode Devices</B></FONT></CENTER></H3>

<BR>

<P>Everything attached to the computer that Linux communicates with is treated as a device. Terminals, printers, and asynchronous modems are character mode devices; they communicate by using characters sent one at a time and echoed back by the other end. Hard drives and most tape drives, on the other hand, use blocks of data, which is the fastest way to send large chunks of information. They are called block mode devices.

<BR>

<P>Some devices can be both character and block mode devices, although not at the same time. Some tape drives, for example, can handle both character and block modes, which means that such drives will have two different device drivers. The device driver that is used depends on how the user wants to read or write data with the device. For the fastest throughput, a block mode device is used. For retrieval of a single file or backing up just a single directory, for example, a character mode device driver is preferable.

<BR>

<BLOCKQUOTE>

<BLOCKQUOTE>

<HR ALIGN=CENTER>

<BR>

<NOTE><A NAME="I2"></A>Another way to differentiate between character and block mode devices is by how the buffering to the device is handled. Character mode devices want to do their own buffering. Block mode devices, which usually communicate in chunks of 512 or 1024 bytes, have the kernel perform the buffering. This buffering is usually transparent to users.</NOTE>

<BR>

<HR ALIGN=CENTER>

</BLOCKQUOTE></BLOCKQUOTE>

<P>The device driver file has all the details about whether the device is a character mode or block mode device. To figure out which type of device a peripheral is, look at the permission block of the device driver file. If the first character of the permission block is a b, the device is a block mode device. A c as the first character in the permission block indicates a character mode device. In the following extract from a /dev directory listing, you can see the device's type from the first character in the permission blocks:

<BR>

<PRE>

<FONT COLOR="#000080">crw-rw---- 1 root uucp 5, 74 Jul 17 1994 cua10

crw-rw---- 1 root uucp 5, 75 Jul 17 1994 cua11

brw-rw---- 1 root floppy 2, 0 Jul 17 1994 fd0

brw-rw---- 1 root floppy 2, 1 Jul 17 1994 fd1

brw-rw---- 1 root disk 3, 0 Jul 17 1994 hda

brw-rw---- 1 root disk 3, 1 Jul 17 1994 hda1

brw-rw---- 1 root disk 8, 1 Jul 17 1994 sda1

brw-rw---- 1 root disk 8, 10 Jul 17 1994 sda10

brw-rw---- 1 root disk 8, 11 Jul 17 1994 sda11

crw--w--w- 1 root root 4, 0 Jul 17 1994 tty0

crw--w--w- 1 root root 4, 1 Jul 7 18:16 tty1

crw--w--w- 1 root root 4, 2 Jul 7 17:58 tty2

crw-rw-rw- 1 root tty 4, 64 Jul 17 1994 ttyS0

crw-rw-rw- 1 root tty 4, 65 Jul 7 18:00 ttyS1</FONT></PRE>

<P>You may notice that this listing has two numbers where the file size usually belongs. These numbers are the major and minor device numbers. The next section explains these numbers.

<BR>

<P>Device drivers are usually named to indicate the type of device they are. Most terminals, for example, have a device driver name tty followed by two or more letters or numbers, such as tty1, tty1A, or tty04. The letters <I>tty</I> identify the file as a terminal (<I>tty</I> stands for teletype), and the numbers or letters identify the specific terminal. When coupled with the directory /dev, the full device driver name becomes /dev/tty01. Hard drives, as you saw in earlier chapters, have names like /dev/hda and /dev/sda (for IDE and SCSI drives respectively).

<BR>

<BR>

<A NAME="E69E69"></A>

<H4 ALIGN=CENTER>

<CENTER>

<FONT SIZE=4 COLOR="#FF0000"><B>Major and Minor Device Numbers</B></FONT></CENTER></H4>

<BR>

<P>A system may have more than one device of the same type. For example, your Linux system may have a multiport card (multiple serial ports) with ten Qume terminals hanging off it. Linux can use the same device driver for each one of the terminals, as they are all the same type of device. The same concept applies to multiple printers, so you can use the same device driver for two Hewlett Packard LaserJet printers attached to two parallel ports, for example.

<BR>

<P>The operating system must have a method of differentiating the ten terminals or two printers, however. Device numbers are that method. Each device is identified by a major number that identifies the device driver to be used and a minor number that identifies the device number. For example, the ten Qume terminals may all use a device driver with the same major number (which really points to the device driver file in the /dev directory), but each has a different minor number that uniquely identifies the Qume terminal to the operating system. In the following listing, you can see that all the device drivers for the ttyX device (which is the console and associated screens) have the same major device number of 4, but the minor device number changes from 0 to 9:

<BR>

<PRE>

<FONT COLOR="#000080">crw--w--w- 1 root tty 4, 0 Jul 17 1994 tty0

crw--w--w- 1 root root 4, 1 Oct 13 13:48 tty1

crw--w--w- 1 root root 4, 2 Oct 13 13:26 tty2

crw--w--w- 1 root root 4, 3 Oct 13 13:26 tty3

crw--w--w- 1 root root 4, 4 Oct 13 13:26 tty4

crw--w--w- 1 root root 4, 5 Oct 13 13:26 tty5

crw--w--w- 1 root root 4, 6 Oct 13 13:26 tty6

crw-rw-rw- 1 root tty 4, 7 Jul 17 1994 tty7

crw-rw-rw- 1 root tty 4, 8 Jul 17 1994 tty8

crw-rw-rw- 1 root tty 4, 9 Jul 18 1994 tty9</FONT></PRE>

<P>In directory listings, the major device number is always shown first, followed by the minor device number. Every device on the system has both major and minor device numbers assigned in such a way as to ensure that they are unique. If two devices are assigned the same number, Linux can't properly communicate with them. You can't create a device number within the device driver or by numbering the files. You create device numbers with the command mknod (make node) and remove them with the rm command. You must use mknod every time you want to configure a new device on your system.

<BR>

<BLOCKQUOTE>

<BLOCKQUOTE>

<HR ALIGN=CENTER>

<BR>

<NOTE>Some devices use the major and minor device numbers in a strange way. Some tape drives, for example, use the minor number to identify the density of the tape in order to adjust their output. These types of exceptions are rare, luckily, and don't occur on most Linux systems. You do not have to create these types of device numbers because an installation script usually handles the setup of the device drivers and their numbers.</NOTE>

<BR>

<HR ALIGN=CENTER>

</BLOCKQUOTE></BLOCKQUOTE>

<BR>

<A NAME="E69E70"></A>

<H4 ALIGN=CENTER>

<CENTER>

<FONT SIZE=4 COLOR="#FF0000"><B>The </B><B>mknod</B><B> Command</B></FONT></CENTER></H4>

<BR>

<P>The mknod (make node) command is used for several different purposes. Its most common usages are to create a FIFO (first in first out) device file, which is a form of queue for the device, or a character or block mode device file. The format of the mknod command is

<BR>

<BR>

<PRE>

<FONT COLOR="#000080">mknod [options] device b|c|p|u major minor</FONT></PRE>

<P>You can use the following options with this command:

<BR>

<UL>

<LI>The --help option displays help information and exits.

<BR>

<BR>

<LI>The -m (or --mode) option sets the mode of the file to mode instead of the default 0666 (symbolic notation only).

<BR>

<BR>

<LI>The -p option lets you set file permissions.

<BR>

<BR>

<LI>The --version option displays version information, and then exits.

<BR>

<BR>

</UL>

<P>The argument after the device or path name specifies whether the file is a block mode device , character mode device , FIFO device (p), or unbuffered character mode device(u). One of these arguments must be present on the command line for the device number to be properly assigned.

<BR>

<P>Following the type of file argument are two numbers for the major and minor device numbers assigned to the new file. You must supply these numbers; mknod does not generate them for you. Every device on a UNIX system has a unique number that identifies the type of device (the major number) and the specific device itself (the minor number). You must specify both a major and minor number for any new block, character, or unbuffered mode devices. You don't need to specify device numbers for a type p device. You can pull the major and minor device numbers out of thin air as long as they don't conflict with any other device, but there is a general numbering convention for all devices. These numbering systems are discussed in more detail in the chapters on specific devices later in this part.

<BR>

<BR>

<A NAME="E68E37"></A>

<H3 ALIGN=CENTER>

<CENTER>

<FONT SIZE=5 COLOR="#FF0000"><B>Device Permissions and Links</B></FONT></CENTER></H3>

<BR>

<P>When you create a new device with the mknod command, it is given the permissions associated with the symbolic value 666. You can override this value either on the mknod command line (with the -p option) or by using chmod afterwards to change the permissions.

<BR>

<P>Device file permissions are important as they can control access to the device. For example, a CD-ROM drive is a read-only device, and changing the permissions to prevent write access can help forestall many error messages when a user tries to write to the CD-ROM. Permissions are also useful for creating read-only partitions on a disk, such as a directory of databases or utilities that you don't want anyone modifying. <A HREF="lsg17.htm">Chapter 17</A>, &quot;System Names and Access Permissions,&quot; looks at the chmod command and file permissions in more detail.

<BR>

<P>As a general rule, the default permissions are valid for most devices unless you want to lock out access. When Linux installs the device files, it sets all the system devices (such as /dev/mem, the physical RAM) with the proper permissions and you shouldn't change them.

<BR>

<BR>

<A NAME="E68E38"></A>

<H3 ALIGN=CENTER>

<CENTER>

<FONT SIZE=5 COLOR="#FF0000"><B>Summary</B></FONT></CENTER></H3>

<BR>

<P>This chapter introduced device drivers and device numbers, both of which are important when you add new hardware to your system. The next few chapters look at the different hardware that you can add to expand your Linux system and how they are configured. This chapter's information will be important as you configure Linux to accept new devices.

<P ALIGN=LEFT>

<A HREF="lsgpt02.htm" TARGET="_self"><IMG SRC="purprev.gif" WIDTH = 32 HEIGHT = 32 BORDER = 0 ALT="Previous Page"></A>

<A HREF="#I0" TARGET="_self"><IMG SRC="purtop.gif" WIDTH = 32 HEIGHT = 32 BORDER = 0 ALT="Page Top"></A>

<A HREF="index.htm" TARGET="_self"><IMG SRC="purtoc.gif" WIDTH = 32 HEIGHT = 32 BORDER = 0 ALT="TOC"></A>

<A HREF="lsg07.htm" TARGET="_self"><IMG SRC="purnext.gif" WIDTH = 32 HEIGHT = 32 BORDER = 0 ALT="Next Page"></A>


</BODY></HTML>



