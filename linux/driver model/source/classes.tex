
\section{Device Classes}

A device class describes a function that a device performs, regardless
of the bus on which a particular device resides. Examples of device
classes are:

\begin{itemize}
\begin{footnotesize}
\item Audio output devices.
\item Network devices.
\item Disks.
\item Input devices.
\end{footnotesize}
\end{itemize}

A device class is characterized by a set of interfaces that allow user
processes to communicate with devices of their type. An interface
defines a protocol for communicating with those devices, which is
usually characterized in a user space device node. A class may contain
multiple interfaces for communicating with devices, though the devices
and drivers of that class may not support all the interfaces of the
class. Device interfaces are described in the next section.

A driver for a device class maintains lists of device and drivers that
belong to that class. They are optional bodies of code that may be
compiled as a module. Its data structures are statically allocated. A
device class defines object types to describe registered
devices. These objects define the devices in the context of the class
only, since classes are independent of the registered devices' bus
type. 

The driver model defines struct device\_class to represent a device
class driver. The driver model does not explicitly represent the
per-device objects that device class drivers operate on, though they
are expected to use the class\_data member of struct device to store
that object. 



\subsection*{Structural Definition}


\begin{table}
\begin{footnotesize}
\begin{tabularx}{\linewidth}{|X|X|X|} \hline
\textit{Name}	& \textit{Return Type} & \textit{Description} \\ \hline \hline

name	& char *	& Name of device class.
\\\hline

devnum	& u32	& Value for enumerating devices registered with
	class. 
\\\hline

subsys	& struct subsystem	& Collection of subordinate
	interfaces. 
\\\hline

devsubsys	& struct subsystem	& Collection of devices
	registered with class. 
\\\hline

drvsubsys	& struct subsystem	& Collection of drivers
	registered with class.	 
\\\hline

devices	& struct list\_head	& List of devices registered with
	class. 
\\\hline

drivers	& struct list\_head	& List of drivers registered with
	class. 
\\\hline

\end{tabularx} \\
\end{footnotesize}
\caption{\footnotesize{struct device\_class Data Fields.}}
\end{table}



\begin{table}
\begin{footnotesize}
\begin{tabularx}{\linewidth}{|X|X|X|} \hline
\textit{Name}	& \textit{Return Type} & \textit{Description} \\ \hline \hline

add\_device(
  struct device *
)
& int
& Method called to register device with class. Called after device has
	been bound to driver belonging to class. 
\\\hline

remove\_device(
  struct device *
)
& void
& Called to unregister device from class. Called while detaching
	device from driver that belongs to class. 
\\\hline

hotplug(
  struct device *dev,
  char **envp,
  int num\_envp,
  char *buffer,
  int buffer\_size
)
& int
& Called before /sbin/hotplug is called when device is registered with
	class. This is opportunity for class to fill in and format
	parameters to /sbin/hotplug. 
\\\hline


\end{tabularx} \\
\end{footnotesize}
\caption{\footnotesize{struct device\_class Methods.}}
\end{table}


Struct device\_class most closely resembles struct bus\_type. Indeed,
many of the fields of struct device\_class server a similar purpose as
those in struct bus\_type. The subordinate subsystem 'devsubsys' and
the 'devices' list manage the list of devices registered with the
class. Like with buses, these structures must exist in parallel, since
a kobject may not belong to more than one subsystem at a time. The
same is true of the 'drvsubsys' and 'drivers' list with regard to
drivers registered with the classes.

The 'subsys' member manages the list of interfaces registered with the
class. As interfaces are registered, they are inserted into the
subsystem's list. This member is also used to register the class with
the kobject hierarchy, as a member of the global class subsystem.

The 'devnum' field is an enumerator for devices that are attached to
the class. A device's 'class\_num' field is set to this value when a
device is registered with a class. The 'devnum' field is
incremented after each time a device is enumerated. 

Device classes contain three methods that are called by the driver
model core during the process of adding and removing devices from a
class. add\_device() is called when a device is registered with the
device class. A device is registered with a device class immediately
after the device is bound to a driver. It is added to the class that
the driver belongs to. remove\_device() is called when a device is
unregistered from a device class. This happens when a device is being
detached from its driver. 

The 'hotplug' method is called immediately after a device is added or
removed from a device class. This allows the class to define
additional environment variables to set for the hotplug agent when the
driver model core executes it. 



\subsection*{Programming Interface}

Device classes have a similar interface to other driver model
objects. It offers calls to register and unregister device classes, as
well as function to adjust the reference count. Device class drivers
should statically allocate struct device\_class objects. They should
initialize the 'name' field, and the pointers of the methods they
support. The object should be registered in the class's initialization
function, and unregistered in the class's tear down function, if it is
compiled as a module.  

\begin{table}
\begin{footnotesize}
\begin{tabularx}{\linewidth}{|X|X|X|} \hline
\textit{Name}	& \textit{Return Type} & \textit{Description} \\ \hline \hline

devclass\_register(
  struct device\_class *
)
& int
& Register class with core.
\\\hline

devclass\_unregister(
  struct device\_class *
)
& void 
& Unregister class with core.
\\\hline

get\_devclass(
  struct device\_class *
)
& struct device\_class * 
& Increment class's reference count.
\\\hline

put\_devclass(
  struct device\_class *
)
& void 
& Decrement class's reference count.
\\\hline

\end{tabularx} \\
\end{footnotesize}
\caption{\footnotesize{struct device\_class Programming Interface.}}
\end{table}

