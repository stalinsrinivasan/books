#my first assembly program

.data


# declaring data (string)

HelloWorldString:

	.ascii "Hello world\n"

#text segment (instructions - do what)

.text

# saying start function declared globally

.global _start

# saying pgm to execute from here - main()

_start:

	# implementing write() system call 	

	movl $4, %eax			# system call (/usr/include/asm/unistd.h)
	movl $1, %ebx			# file descriptor - STDOUT - 1  
	movl $HelloWorldString, %ecx	# buffer pointer
	movl $12, %edx			# buffer length
	int $0x80			# raising s/w INTR

	# implementing exit() system call 
	movl $1, %eax
	movl $0, %ebx
	int $0x80


