# get down to variable types

.data

	HelloWorld:
			.ascii "Hello World!"
	
	Byte:
			.byte 10
		
	Int32:
			.int 2

	Int16:
			.short 10

	Float:
			.float 10.23

	IntegerArray:
			.int 10,20,30,40,50

.bss
	.comm LargeBuffer 10000

.text

	.global _start

	_start:
		
			nop

			# mov immediate value to register

			movl $10, %eax


			# mov immediate value to mry location

			movw $50, Int16 


			# mov data bw regs

			movl %eax, %ebx


			# mov data bw mry and reg

			movl Int32, %eax


			# mov reg to mry

			movb $3, %al

			movb %al, Byte


			# mov data to indexed mry location

			movl $0, %ecx
			
			movl $2, %edi

			movl $22, IntegerArray(%ecx, %edi, 4)

			# indirect addressing

			movl $Int32, %eax

			movl (%eax), %ebx

			movl $9, (%eax)


			# exit the pgm
				
			movl $1, %eax

			movl $10, %ebx

			int $0x80

		
	
	
