.data
	HelloWorld:
		
		.asciz	"Hello World!\n"

	CallMeDemo:

		.asciz "call works!\n"
.text

.global _start

	_start:

		nop
		
		call CallMe			# call CallMe and return to it check eip and esp
	
		jmp ExitProgram			# jmp to the label, replacing eip to <label.addr> check eip

	WriteProgram:

		movl $4, %eax
		movl $1, %ebx
		movl $HelloWorld, %ecx
		movl $14, %edx
		int $0x80 


	ExitProgram:

		movl $1, %eax
		movl $10, %ebx
		int $0x80

			
	CallMe:

		movl $4, %eax
		movl $1, %ebx
		movl $CallMeDemo, %ecx
		movl $13, %edx
		int $0x80 
		ret
