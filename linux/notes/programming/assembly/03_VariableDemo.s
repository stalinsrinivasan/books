# get down to variable types

.data

	HelloWorld:
		.ascii "Hello World!"
	
	Byte:
		.byte 10
		
	Int32:
		.int 2

	Int16:
		.short 10

	Float:
		.float 10.23

	IntegerArray:
		.int 10,20,30,40,50

.bss
	.comm LargeBuffer 10000

.text

	.global _start

		_start:
				
			movl $1, %eax
			movl $0, %ebx
			int $0x80

		
	
	
