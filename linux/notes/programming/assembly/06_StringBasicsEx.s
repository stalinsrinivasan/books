.data

	HelloWorldString:
		
		.asciz "Hello World of Assembly!"

	H311O:
		
		.asciz "Hello Woold of AsSembly!"

.text

	.global _start

	_start:

		nop
	
		# comparing two strings pointed by esi and edi and results in eflags reg

		leal HelloWorldString, %esi	# load effective addr of string mry loc to esi in long format (4b)

		leal H311O, %edi

		cmpsb
		cmpsb
		cmpsb
		cmpsb
		cmpsb
		cmpsb
		cmpsb

		repz cmpsb

		# exit the function

		movl $1, %eax

		movl $0, %ebx

		int $0x80
