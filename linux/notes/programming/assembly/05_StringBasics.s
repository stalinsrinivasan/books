.data

	HelloWorldString:
		
		.asciz "Hello World of Assembly!"

	H311O:
		
		.asciz "H311O"

.bss

	.lcomm Destination, 100
	.lcomm DestinationUsingRep, 100
	.lcomm DestinationUsingStos, 100

.text

	.global _start

	_start:

		nop
	
		# 1. moving data from mry using movsb, movsw, movsl

		# storing source and destination in esi and edi for mry operations

		movl $HelloWorldString, %esi

		movl $Destination, %edi	

		# movb move data byte by byte

		movsb

		# movb move data by 2 bytes

		movsw
	
		# movb move data by 4 bytes

		movsl

		# set and clear direction flag (DF)


		std # by setting direction flag we can decrement esi, edi after mry operations

		cld # by clearing direction flag we can increment esi, edi after mry operations


		# memory operation using REP cmd

		 movl $HelloWorldString, %esi

		 movl $DestinationUsingRep, %edi

		 movl $25, %ecx			# loading str length in ecx

		 rep movsb			# doing movsb till ecx becomes zero



		# memory operation from mry to register (eax)

		# lodsb, lodsw, lodsl always load to eax and source should be in esi

		leal HelloWorldString, %esi	# load effective addr of string mry loc to esi in long format (4b)

		lodsb				# load value to al which esi pointing

		movb $0, %al			# resetting the written value	

		
		dec %esi			# decrementing esi ie inc to next byte

		lodsw				# load value to ax

		movw $0, %ax

		
		subl $2, %esi			# decr esi ie inc to next 2 bytes

		lodsl				# load value to eax

		subl $4, %esi			# decr esi ie inc to next 4 bytes

		
		# memory optn from reg to mry

		# stosb, stosw, stosl always load to memory point by edi from eax reg

		leal DestinationUsingStos, %edi

		stosb

		dec %edi

		stosw

		subl $2, %edi

		stosl

		# comparing two strings pointed by esi and edi and results in eflags reg

		leal HelloWorldString, %esi	# load effective addr of string mry loc to esi in long format (4b)

		leal H311O, %edi

		cmpsb

		dec %esi
	
		dec %edi

		cmpsw

		subl $2, %esi

		subl $2, %edi

		cmpsl

		# exit the function

		movl $1, %eax

		movl $0, %ebx

		int $0x80
