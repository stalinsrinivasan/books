GPIO interface:

gpio -- processor pins other than address, data and some dedicated controller pins

     -- these pins can be put down as i/p or o/p pin as we wish

     -- there also gpio expander which gives this functionality as enhancement of processor gpio pin

     -- gpio expander which is connected to processor thru i2c (i2c i/o expander) which is attached to other interfaces \

        such as nand, nor, spi, mmc in the other end.

     -- provides the way for processor and peripheral for their communication between them

     -- so porting requires gpio changes duly on interface we are referring


50 pin connector --> gpio interface

32 --> data I/O lines (16 - data Input <-> 16 - data Output )

3  --> handshaking lines ( PCTL , PFLG, IO )

       PCTL - Peripheral ConTrol Line - used by processor to initiate data transfer

       PFLG - Peripheral FLaG - used by peripheral to ack the transfer

       IO   - Input/Output line - indicates the direction of data transfer

3  --> special purpose lines ( EIR, PSTS, PRESET )

      EIR    - External Interrupt Request - used by peripheral to raise INTR to processor

      PSTS   - Peripheral STatuS - to represent the status of peripheral

      PRESET - Peripheral RESET  - to reset the peripheral

4  --> general purpose lines ( CTL0, CTL1, STI0, STI1 )

      CTL0, CTL1 - two ctrl lines that processor can set

     STI0, STI1  - two status lines that processor can read

6  --> ground lines

2  --> not defined/connected
